const container = document.getElementById("container");
 
 fetch("https://ajax.test-danit.com/api/swapi/films")

   .then(response => response.json())

   .then(data => {
      // console.log(data);
      const filmsList = document.createElement("ul");
      filmsList.classList.add("films");
      container.appendChild(filmsList);
      data.forEach(film => {
         const filmsItem = document.createElement("li");
         filmsItem.innerHTML = `
         <p><b>Episode: </b>${film.episodeId}</p>
         <p><b>Name: </b>${film.name}</p>
         <p><b>Opening Crawl: </b>${film.openingCrawl}</p>
         <p><b>Список персонажів: </b></p>
         <div class="loading"></div>
         `;
         filmsList.appendChild(filmsItem);
       });
       return data;
   })

   .then(data => {
      const filmsItems = container.querySelectorAll("li");
      const loadingAnimations = document.querySelectorAll(".loading");

      for (let i = 0; i < data.length; i++) {
         const charactersUrls = data[i].characters;

         const charactersList = document.createElement("ul");
         charactersList.classList.add("characters");

         let j = 0;

         for (const url of charactersUrls) {
            fetch(url)

            .then(response => response.json())

            .then(character => {
               const charactersItem = document.createElement("li");
               charactersItem.innerHTML = character.name;
               charactersList.appendChild(charactersItem);

               j++
               if (j == charactersUrls.length) {
                  loadingAnimations[i].style.display = "none";
                  filmsItems[i].appendChild(charactersList);
               }
            })

            .catch(error => {
               console.error("Помилка при завантаженні списку персонажів: ", error);
             });
         }
      }
   })

   .catch(error => {
     console.error("Помилка: ", error);
   });






